# use-case

- John buys an offer for it's family
- the offer is a service that is shared by all its family members (let's say 20 films on VOD, renewed each month on subscription anniversary)
- there is a bucket shared among all the users, it never should be exceeded
